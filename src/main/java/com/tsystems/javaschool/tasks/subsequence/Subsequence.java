package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (null == x || null == y )
            throw new IllegalArgumentException();
        if (x.size() > y.size())
            return false;

        if (x.isEmpty()) return true;

        int xPointer = 0;

        for (Object yElement:y) {
            if(xPointer<x.size()){
                if (null != yElement && yElement.equals(x.get(xPointer)))
                    ++xPointer;
            } else break;
        }

        return xPointer == x.size();
    }
}
