package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (null == statement || statement.isEmpty()) return null;
        List<Token> tokens = parser.splitToToken(statement);
        if (null == tokens || tokens.isEmpty()) return null;

        for (Token curToken:tokens) {

            if (curToken.getType() == Type.NUMBER) {
                valueStack.push(curToken);
            } else if (curToken.getType() == Type.OPERATOR) {
                if (operatorStack.isEmpty() || curToken.getRating() > operatorStack.peek().getRating()) {
                    operatorStack.push(curToken);
                } else {
                    while (!operatorStack.isEmpty() && curToken.getRating() <= operatorStack.peek().getRating()) {
                        Token toProcess = operatorStack.poll();
                        processOperator(toProcess);
                    }
                    operatorStack.push(curToken);
                }
            } else if (curToken.getType() == Type.LEFT_PARENTHESIS) {
                operatorStack.push(curToken);
            } else if (curToken.getType() == Type.RIGHT_PARENTHESIS) {
                while (!operatorStack.isEmpty() && operatorStack.peek().getType() == Type.OPERATOR) {
                    Token toProcess = operatorStack.poll();
                    processOperator(toProcess);
                }
                if (!operatorStack.isEmpty() && operatorStack.peek().getType() == Type.LEFT_PARENTHESIS) {
                    operatorStack.poll();
                } else {
                    error = true;
                }
            }

        }

        while (!operatorStack.isEmpty() && operatorStack.peek().getType() == Type.OPERATOR) {
            Token toProcess = operatorStack.poll();
            processOperator(toProcess);
        }

        if(error == false) {
            Token result = valueStack.poll();
            if (!operatorStack.isEmpty() || !valueStack.isEmpty()) {
                return null;
            } else {
                if (Double.isNaN(result.getValue()) || Double.isInfinite(result.getValue()))
                    return null;
                return format(result.getValue());
            }
        }
        return null;
    }

    private Deque<Token> operatorStack;
    private Deque<Token> valueStack;
    private TokenParser parser;

    public Calculator() {
        operatorStack = new ArrayDeque<>();
        parser = new TokenParser();
        valueStack = new ArrayDeque<>();
    }

    private boolean error = false;

    private void processOperator(Token t) {
        Token leftToken = null, rightToken = null;
        if (valueStack.isEmpty()) {
            error = true;
            return;
        } else {
            rightToken = valueStack.poll();
        }
        if (valueStack.isEmpty()) {
            error = true;
            return;
        } else {
            leftToken = valueStack.poll();
        }
        Token result = t.operate(leftToken.getValue(), rightToken.getValue());
        valueStack.push(result);
    }


    public String format(double value) {
        if (value == (int)value)
            return String.valueOf((int)value);
        Double res = new BigDecimal(value).setScale(4, RoundingMode.HALF_UP).doubleValue();
        return res.toString();
    }


}
