package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by Home on 24.01.2019.
 */
public enum Type {

    UNKNOWN(-1),
    NUMBER(0),
    OPERATOR(1),
    LEFT_PARENTHESIS(2),
    RIGHT_PARENTHESIS(3);

    private int rating;

    Type(int rating) {
        this.rating = rating;
    }

    public int getRating() { return rating; }

}